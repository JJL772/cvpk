
CC?=gcc
CFLAGS+=-O3 -g 
LIBS+=-lc 

all:
	$(CC) $(CFLAGS) -o cvpk $(wildcard src/*.c) $(LIBS)

clean:
	rm cvpk 
