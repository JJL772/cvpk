/**
 *
 * cvpk.h - VPK Library in pure C 
 *
 */ 
#pragma once

#include <stdint.h>
#include <stddef.h>

#define VPK_FILE_SIGNATURE 0x55AA1234
#define VPK_DIRENT_TERMINATOR 0xFFFF

/* Basic data structures */
typedef struct vpk2_header_s {
	uint32_t signature;
	uint32_t version;
	uint32_t tree_size;
	uint32_t file_data_size;
	uint32_t archive_md5_size;
	uint32_t other_md5_size;
	uint32_t signature_size;
} vpk2_header_t;

typedef struct vpk_header_s {
	uint32_t signature;
	uint32_t version;
	uint32_t tree_size;
} vpk_header_t;

typedef struct vpk2_dirent_s {
	uint32_t crc32;
	uint16_t preload;
	uint16_t archive_index;
	uint32_t entry_offset;
	uint32_t entry_length;
	uint16_t terminator; // = 0xFFFF;
} vpk2_dirent_t;

typedef struct vpk2_archive_md5_entry_s {
	uint32_t archive_index;
	uint32_t start_offset;
	uint32_t count;
	char checksum[16];
} vpk2_archive_md5_entry_t;

typedef struct vpk2_other_md5_entry_s {
	char tree_checksum[16];
	char archive_checksum[16];
	char unknown[16];
} vpk2_other_md5_entry_t;

/* NOTE: VPK2 files have a fixed publickey/signature size, at least according to the wiki */
/* Some custom internal handling of signatures/keys should be applied, probably */
/* VDC wiki link: https://developer.valvesoftware.com/wiki/VPK_File_Format#Public_Key_.26_Signature */
typedef struct vpk2_signature_s {
	/* Publickey is always 160 bytes, apparently */
	uint32_t publickey_size;
	char publickey[160];

	/* Signature is always 128 bytes apparently */
	uint32_t signature_size;
	char signature[128];
} vpk2_signature_t;

typedef struct vpk_settings_s {
	void(*malloc)(size_t);
	void(*free)(void*);
} vpk_settings_t;

typedef char vpk2_checksum_t[16];

typedef struct vpk_file_handle_s vpk_file_handle_t;
typedef struct vpk_s vpk_t;
typedef struct vpk2_file_handle_s vpk2_file_handle_t;
typedef struct vpk2_s vpk2_t;


/**
 * Reads a VPK2 file from the disk. 
 * @param path Path to the vpk DIRECTORY file. If the path is not postfixed with _dir.vpk, _dir.vpk will be appended.
 * 		Some valid paths: path/to/vpk_dir.vpk path/to/vpk.vpk path/to/vpk
 * @param settings Structure of settings. If any params in the settings struct are NULL, default ANSI C functions will be used 
 */ 
vpk2_t* vpk2_read(const char* path, vpk_settings_t settings);

/**
 * Creates an empty vpk2 archive in memory.
 * @param name base name of the VPK file. e.g. the file in file_dir.vpk
 * @param size_per_archive target size, in bytes, of each archive file. The resultant size will not exceed this.
 */ 
vpk2_t* vpk2_new(const char* name, size_t size_per_archive);

/**
 * Returns if the vpk2 file is dirty and needs to be flushed to disk or not.
 * @return 0 if clean, 1 if dirty
 */ 
int vpk2_dirty(vpk2_t* vpk);

/**
 * Returns a handle to the specified VPK file. NULL is returned 
 * if the file cannot be found. 
 * @param Full path to the file, with extension
 * @return Returns a handle to the file 
 */ 
vpk2_file_handle_t* vpk2_get_file(vpk2_t* vpk, const char* full_path);

/**
 * Creates a new file entry in the vpk file.
 * @param full_path Full "path" of the file.
 * @return Handle to the new entry. If some error happens, NULL is returned. NULL is also returned if the file already
 * 	exists in the archive. 
 */ 
vpk2_file_handle_t* vpk2_create_file(vpk2_t* vpk, const char* full_path);

/**
 * Use to iterate through the files inside of a VPK.
 * This does NOT iterate in any particular order, so do NOT depend on that.
 * @param vpk The VPK file
 * @param iter Iterator. Simply pass this a pointer to a void* parameter on the stack. Stores
 * 		Implementation-defined data to allow for iteration
 * @return File handle for the file, or NULL if no more left
 */ 
vpk2_file_handle_t* vpk2_iterate_files(vpk2_t* vpk, void** iter);

/**
 * @return The number of files in the VPK archive.
 */ 
size_t vpk2_get_num_files(vpk2_t* vpk);

/**
 * Gets the MD5 checksum of the file
 * @param file The file to get the checksum of
 * @param checksum Buffer to write checksum to (must be 16 bytes in size)
 * @return 0 if checksum is written to checksum, 1 if some error happened 
 */ 
int vpk2_file_get_checksum(vpk2_file_handle_t* file, vpk2_checksum_t checksum);

/**
 * Verifies the checksum of the specified vpk file
 * @param file File to check checksum of
 * @return 0 if checksum is OK, 1 if it does not match
 */ 
int vpk2_file_check_checksum(vpk2_file_handle_t* file);

/** 
 * Gets the size of the file
 * @param file VPK file to use 
 * @return Returns the total size of the file on disk, including preload bytes
 */ 
size_t vpk2_file_get_size(vpk2_file_handle_t* file);

/**
 * @return Returns the number of preload bytes in the file 
 */ 
size_t vpk2_file_get_preload_size(vpk2_file_handle_t* file);

/**
 * Reads the preload bytes of the file 
 * @param file File
 * @param outbuf Buffer to write output file data
 * @param size_of_outbuf Size of the buffer in bytes, to prevent overflows
 * @return Number of bytes read, or 0 if none 
 */ 
size_t vpk2_file_get_preload(vpk2_file_handle_t* file, void* outbuf, size_t size_of_outbuf);

/**
 * Reads the file from disk or memory. This makes sure to concatenate preload and non-preload data
 * @param file File
 * @param outbuf Buffer to write the file data to
 * @param size_of_outbuf Size, in bytes, of the output buffer
 * @return Number of bytes read, or 0 if none were read.
 */ 
size_t vpk2_file_read(vpk2_file_handle_t* file, void* outbuf, size_t size_of_outbuf);

/**
 * Sets the number of preload bytes the file should use. 
 * @param file VPK file
 * @param bytes Number of preload bytes. This will be limited to 16kb
 */ 
void vpk2_file_set_preload(vpk2_file_handle_t* file, size_t bytes);

/**
 * Sets the file data of the vpk file
 * @param file File to set data for 
 * @param buffer The buffer of data to use
 * @param size Size of the buffer
 * @param flush If true, flush the file data to disk to avoid massive memory usage
 */ 
void vpk2_file_set_data(vpk2_file_handle_t* file, void* buffer, size_t size, bool flush);

/**
 * Clears the data for the file
 * @param file File to clear data for 
 */ 
void vpk2_file_clear_data(vpk2_file_handle_t* file);


